<?php
if (getenv('S3_HOSTNAME') && getenv('S3_BUCKET') && getenv('S3_ACCESS_KEY')
  && getenv('S3_SECRET_KEY') && getenv('S3_PORT') && getenv('S3_REGION')
) {
  $CONFIG = array (
    'objectstore' => array(
      'class' => '\\OC\\Files\\ObjectStore\\S3',
      'arguments' => array(
              'bucket' => getenv('S3_BUCKET'),
              'autocreate' => true,
              'key'    => getenv('S3_ACCESS_KEY'),
              'secret' => getenv('S3_SECRET_KEY'),
              'hostname' => getenv('S3_HOSTNAME'),
              'port' => getenv('S3_PORT'),
              'use_ssl' => true,
              'region' => getenv('S3_REGION'),
      ),
    ),
  );
}
